package com.yuan.generatorcode.test;

import com.yuan.generatorcode.config.DatabaseConfig;
import com.yuan.generatorcode.config.GeneratorConfig;
import com.yuan.generatorcode.config.GlobalConfig;
import com.yuan.generatorcode.core.GeneratorCode;

/**
 * Explain:文件生成测试
 * Date:2017/12/13 13:58
 * Coder:YouYuan
 * Version:1.0
 */
public class GeneratorTest {
    public static void main(String[] args) {
//        mysqlTest();
        oracleTest();
    }

    private static void oracleTest() {
        GeneratorConfig generatorConfig = new GeneratorConfig();// 文件生成信息配置
        generatorConfig.setAuthor("YouYuan");// 作者，用于生成注释
        generatorConfig.setPackageName("com.yuan");// 包名
        generatorConfig.setOutputPath("D:/Temp/AGCode/Test2.2/");// 设置文件输出路径

        DatabaseConfig databaseConfig = new DatabaseConfig();// 数据库信息配置
        String dbName = "db";// 数据库名
        databaseConfig.setDbUrl("jdbc:oracle:thin:@192.168.1.8:1521:db");// 数据库连接地址
        databaseConfig.setDbName(dbName);// 设置数据库名
        databaseConfig.setUsername("admin");// 连接数据库的用户名，此处最好使用具有root权限的用户
        databaseConfig.setPassword("1234");// 数据库用户密码
        databaseConfig.setTableName("MEMBER_INFO");
//		databaseConfig.setTablePrefix("t_");// 数据库表名前缀，用于生成类时自动去除此前缀

        GeneratorCode generatorCode = new GeneratorCode(databaseConfig,
                generatorConfig);// 根据配置创建文件生成核心对象
//        generatorCode.batchGenerator();// 全库批量生成
        generatorCode.generator();
    }

    private static void mysqlTest() {
        GeneratorConfig generatorConfig = new GeneratorConfig();
        generatorConfig.setAuthor("YouYuan");
        generatorConfig.setPackageName("com.yuan");
        generatorConfig.setTemplateRoot("~/defaultTemplate/SSM");//使用~代表从classpath读取模板
        generatorConfig.setOutputPath("F:/Temp/AutoGeneratorCode/Test2.1/Common");
        DatabaseConfig databaseConfig = new DatabaseConfig();
        String dbName = "generator";//数据库名
        databaseConfig.setTableName("t_member,t_meng_li");
        databaseConfig.setDbUrl("jdbc:mysql://localhost:3306/" + dbName + "?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true");
        databaseConfig.setDbName(dbName);//设置数据库名
        databaseConfig.setUsername("youyuan");
        databaseConfig.setPassword("123");
//        databaseConfig.setRemoveTables("t_member,t_tian_he");
        databaseConfig.setTablePrefix("t_");
        GlobalConfig globalConfig = new GlobalConfig();
//        globalConfig.setOpenFileManager(true);
//        globalConfig.setTemplateStatementStart("<%");
//        globalConfig.setTemplateStatementEnd("%>");
        GeneratorCode generatorCode = new GeneratorCode(databaseConfig, generatorConfig, globalConfig);//写法一，使用配置信息创建文件生成核心类
//        generatorCode.setDatabaseConfig(databaseConfig) //写法二，应用数据库设置
//                .setGeneratorConfig(generatorConfig) //应用文件生成信息配置
//                .setGlobalConfig(globalConfig); //应用全局配置
//        generatorCode.batchGenerator();//执行批量生成
        generatorCode.generator();//执行指定表生成
//        List<GeneratorInfo> buildData = generatorCode.getBuildData();//获取构建数据集
//        System.out.println(buildData);
    }
}
